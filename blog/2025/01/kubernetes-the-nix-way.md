title="Kubernetes the Nix way"
subtitle="because it wasn't hard enough the first time"
description="a system may only ever grow in complexity, forever"
date=2025-01-13
tags=['kubernetes', 'nix', 'openssl']
::===::
this post is incomplete, you can help by not being rude about it

## chosen ranges
we have to decide some IP address ranges up front so that we can - among other
things - issue correct certs for them

- Internal Node IPs: `10.100.0.0/16` and `2001:db8:b::/48`
- Internal Pod IPs: `10.200.0.0/16` and `2001:db8:c::/48`
- Internal Service IPs: `10.32.0.0/16` and `2001:db8:d::/48`

## generate TLS certificates
### the CA
this will sign all the other certs. keep it very safe. defend it with your life

`ca.conf`:
```
[req]
prompt = no
distinguished_name = req_distinguished_name
x509_extensions = ca_x509_extensions

[ca_x509_extensions]
basicConstraints = CA:TRUE
keyUsage = cRLSign, keyCertSign

[req_distinguished_name]
# Change all of these to fit purpose.
countryName = US
localityName = Brooklyn
commonName = Transcendental Transit
emailAddress = ssl@tta.wtf
```

```bash
# shor's algorithm isn't real and can't hurt you
openssl genrsa -out ca.key 4096
openssl req -x509 -new -sha512 -noenc -key ca.key -out ca.crt -days 3653 -conf ca.conf
```

Check `ca.crt`:
```bash
openssl x509 -in ca.crt -noout -text
```

### the service account signing certificate
service accounts are authorized using JWTs signed by a dedicated TLS
certificate (which is itself signed by the CA we made earlier)

`service-accounts.conf`:
```
[req]
prompt = no
distinguished_name = req_distinguished_name
# request extensions
req_extensions = default_req_extensions

[req_distinguished_name]
CN = service-accounts

[default_req_extensions]
basicConstraints = CA:FALSE
extendedKeyUsage = clientAuth
keyUsage = critical, digitalSignature, keyEncipherment
subjectKeyIdentifier = hash
```

```bash
openssl genrsa -out service-accounts.key 4096
openssl req -new -key "service-accounts.key" -sha512 \
    -config "service-accounts.conf" -out "service-accounts.csr"
openssl x509 -req -days 3653 -in service-accounts.csr \
    -copy_extensions copyall -sha256 \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -out service-accounts.crt
```

### `kube-apiserver`
the API server! it's a great big HTTPS service, of course it gets certificates.
but it also does mutual TLS to `etcd`. we'll use the same certificate for both
purposes because that makes life easier

we will run through the whole "lifecycle" of:
1. creating a key
2. generating a signing request "asking" our "CA" to sign the key
3. fulfilling(?) the signing request with our "CA" using `openssl req` as a
   "micro CA"

`kube-apiserver.conf`:
```
[req]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = default_req_extensions

[req_distinguished_name]
CN = kubernetes

[default_req_extensions]
basicConstraints = CA:FALSE
extendedKeyUsage = clientAuth, serverAuth
keyUsage = critical, digitalSignature, keyEncipherment
subjectAltName = @alt_names
subjectKeyIdentifier = hash

[alt_names]
# Various domain names used to reach the API internally.
DNS.0 = kubernetes
DNS.1 = kubernetes.default
DNS.2 = kubernetes.default.svc
DNS.3 = kubernetes.default.svc.cluster
# Localhost.
IP.0 = 127.0.0.1
IP.1 = ::1
# Internal service IPs. Chosen from the service IP ranges above.
IP.2 = 10.32.0.1
IP.3 = 2001:db8:d::1
```

```bash
openssl genrsa -out kube-apiserver.key 4096
openssl req -new -key "kube-apiserver.key" -sha512 \
    -config "kube-apiserver.conf" -out "kube-apiserver.csr"
openssl x509 -req -days 3653 -in kube-apiserver.csr \
    -copy_extensions copyall -sha256 \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -out kube-apiserver.crt
```

### the "admin" cert
this is a client certificate that will identify the "admin" user to the cluster
later. 

`admin.conf`:
```
[req]
prompt = no
distinguished_name = req_distinguished_name
# copy-paste the [default_req_extensions] from service-accounts.conf above
req_extensions = default_req_extensions

[req_distinguished_name]
# "user" name
CN = admin
# group name. super turbo admin, bypasses all access controls
O = system:masters
```

```bash
openssl genrsa -out admin.key 4096
openssl req -new -key "admin.key" -sha512 \
    -config "admin.conf" -out "admin.csr"
openssl x509 -req -days 3653 -in admin.csr \
    -copy_extensions copyall -sha256 \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -out admin.crt
```

### worker node certs
we need a cert for each node in the cluster. unfortunately OpenSSL is a
nightmare so I don't know a great way to do this other than "copy-paste the
config with different names a bunch of times"

note: "each node" includes control plane nodes! they end up running the kubelet
too. this means you will probably need two of these, minimum. consider writing
a script to automate the process.

for each `NODE`, make a `NODE.conf` like so
```
# replace NODE with the node's hostname
# replace IPV4 with the node's IPv4 address (in 10.100.0.0/16)
# replace IPV6 with the node's IPv6 address (in 2001:db8:b::/48)
[req]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = node_req_extensions

[req_distinguished_name]
# hardcoded magic CN and O values, used to auth kubelet -> apiserver
CN = system:node:NODE
O  = system:nodes

[node_req_extensions]
basicConstraints     = CA:FALSE
extendedKeyUsage     = clientAuth, serverAuth
keyUsage             = critical, digitalSignature, keyEncipherment
subjectAltName       = DNS:NODE, DNS:NODE.kubenet, IP:IPV4, IP:IPV6
subjectKeyIdentifier = hash
```
and run the cert issuance dance:
```bash
openssl genrsa -out NODE.key 4096
openssl req -new -key "NODE.key" -sha512 \
    -config "NODE.conf" -out "NODE.csr"
openssl x509 -req -days 3653 -in NODE.csr \
    -copy_extensions copyall -sha256 \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -out NODE.crt
```
