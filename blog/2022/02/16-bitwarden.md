title="Self-Hosting Bitwarden on Kubernetes"
subtitle="by reading entirely too much YAML"
description="because what else would a college student do over a weekend with no plans"
tags=['bitwarden', 'kubernetes', 'docker-compose', 'microsoft sql server', 'should have used vaultwarden']
date=2022-02-16
::===::
So [Bitwarden][] is pretty neat. It's a completely open-source (AGPLv3!)
password manager that works basically everywhere. At the very least, it works
everywhere I need it to (Firefox and Android). And because it's open source, I
can host it myself! There's much less of a need to trust Bitwarden themselves
when I know exactly where the data lives and can throw it out of a window if I
want.

Although it's not super well-advertised, Bitwarden does properly support
hosting your own instance "on-premises". Unfortunately, this support is
provided somewhat strangely: as a [shell script][] which generates (among other
things) a [Docker Compose][] manifest. The essential idea, then, seems to be
that you spin up a server or VM or what have you, run the installer, and then
that's your Bitwarden server. If you want to restart or update the server,
that's done through the shell script as well.

Unfortunately, beyond "just run the installer!", documentation gets sparse
fast. Even the page on [deploying Bitwarden "manually"][manual-setup] still
uses `docker compose` in the end. Documentation on what container does what, or
what each volume mounted to the containers is used for, ranges from scant to
nonexistent.

With all those warnings in front of us, let's blindly proceed past them and try
to shoehorn this on to Kubernetes anyway!

## random container surprises
### architecture
Most clusters won't even notice this, but all of Bitwarden's images are for
`linux/amd64` only. So if you only have Raspberry Pis, you'll need to get some
x86_64 hardware into your cluster. In theory you could rebuild the images for
other arches - after all, [the `Dockerfile`s are on GitHub like everything
else.][dockerfile-api] But they're `FROM` images provided by Microsoft
(Bitwarden is .NET C#), which are only available for x86, so you'd have to
build that yourself too and yes, [you could rebuild that too][dotnet-docker],
but there's a point at which this becomes Very Silly. Unlike the idea of
running your own, single-user password manager on one of the most advanced
orchestration systems available to the general public, that's completely
reasonable.

A much easier solution might be to use [vaultwarden][], but that's an entirely
different piece of software.

### database setup
At time of writing, Bitwarden only supports Microsoft SQL Server for the
backend data storage. This is... certainly a choice they've opted to make.
There's [some work to support Postgres and MySQL,][pg-mysql] but it's still in
progress.

But at least they provide [a container image][mssql-bw-image] for it. Surely
since they've done that, rather than use the image Microsoft publishes
directly, it'll do basic first-time setup or something, right?

... Right?

As far as I can tell, the easiest way to set up the database is to run
Bitwarden briefly through the installer, then transfer the created database
into the `PersistentVolume` that you mount to the `bitwarden/mssql` container
in your cluster. There are some docs on [setting up the database
yourself][self-db] on an external MSSQL database, but you'd have to know how to
use MSSQL to do that, and [the whole point here is to glue together stuff we
don't understand.][xkcd-docker]

(Again, [vaultwarden][] has support for most common databases - somewhat
ironically _not_ including MsSQL.)

### internal identity access
The API container will attempt to reach Bitwarden's identity service (i.e.
another container) at `http://identity:5000`. As far as I can tell, this can't
be changed with any of the environment variables used for configuration. Just
make sure the relevant `Service` is:
* in the same namespace as the API container
* named `identity` exactly
* listening on port 5000

Just keep it simple and you'll probably be fine.
```yaml
apiVersion: v1
kind: Service
metadata:
  name: identity
  labels:
    app: identity
spec:
  selector:
    app: identity
  ports:
  - protocol: TCP
    port: 5000
    name: http
```

### Nginx/`Ingress` madness
Quick, what does this Nginx snippet do? Specifically, what URI does the service
at `api:5000` see when we make a request such as `GET /api/alive`?
```
location /api/ {
    proxy_pass http://api:5000/;
}
```

Here's a hint: this snippet does something _completely different_:
```
location /api/ {
    proxy_pass http://api:5000;
}
```

The first snippet is taken directly from the Nginx config that Bitwarden's
installer writes. With that config, when Nginx sees a request starting with
`/api/`, it **strips `/api/` from the request URI** before passing it to the
backend. To quote [Nginx's documentation][proxy-doc]:

> If the `proxy_pass` directive is specified with a URI, then when a request is
> passed to the server, the part of a [normalized][] request URI matching the
> location is replaced by a URI specified in the directive:
>
> ```nginx
> location /name/ {
>     proxy_pass http://127.0.0.1/remote/;
> }
> ```

<!-- quell vim ` -->

So in the example from Bitwarden's Nginx configuration, the request for
`/api/alive` would be rewritten as `/alive` before reaching the backend.

If you wanted to convert this to an `Ingress` object inside Kubernetes, you
might write something like the following:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: bitwarden
spec:
  rules:
  - host: bitwarden.example.com
    http:
      paths:
      - path: '/api/'
        backend:
          service:
            name: "api"
            port:
              name: http
        pathType: Prefix
```
But you would quickly find that this doesn't work: all your requests to
`/api/alive` (or any other URI starting with `/api/`) mysteriously 404. Because
`Ingress`es don't alter the request URI before forwarding to the backend
`Service` (at least, by default in compliant `Ingress` Controllers). So the
`api` `Service` sees `GET /api/alive`, but (as you can tell from Bitwarden's
generated Nginx config working out of the box), the service is expecting
requests for just `/alive` - no `/api/` prefix.

Fixing this depends on what `Ingress` controller you have deployed. For the
reasonably popular [`kubernetes/ingress-nginx`][k8s-nginx], you can use the
`nginx.ingress.kubernetes.io/rewrite-target` annotation:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: bitwarden
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
  - host: bitwarden.example.com
    http:
      paths:
      - path: '/api(/|$)(.*)'
        backend:
          service:
            name: "api"
            port:
              name: http
        pathType: Prefix
```
I'm not sure how well I like this version, but it does the job. The same thing
applies to each of the various backend containers Bitwarden uses.

## random volume nonsense
* If the volume looks like it's for logging, that's because it is. The
  containers also log to standard output and standard error, so you should be
  able to use simple `emptyDir` volumes for the logs (and let the logging work
  be handled by cluster infrastructure as normal, whatever that is for your
  setup).
* The `ca-certificates` volume is used to install any additional certificates
  the containers will need as a result of e.g. an organization-wide HTTPS
  proxy. If you don't do that, you can again use an `emptyDir`.

## closing
I know this isn't a full tutorial, but it should help with some of the less
obvious gotchas. Ideally I'd present a magnificent Helm chart here that takes
care of all this nonsense automatically, but that sounds like way more effort
than it's worth (especially with classes to take care of and what).

[Bitwarden]: https://bitwarden.com/
[shell script]: https://bitwarden.com/help/install-on-premise-linux/#install-bitwarden
[Docker Compose]: https://docs.docker.com/compose/
[manual-setup]: https://bitwarden.com/help/install-on-premise-manual/
[dockerfile-api]: https://github.com/bitwarden/server/blob/master/src/Api/Dockerfile
[dotnet-docker]: https://github.com/dotnet/dotnet-docker
[pg-mysql]: https://github.com/bitwarden/server/commit/b13dda279974aff3bec9a758dbb98756335d8fad
[mssql-bw-image]: https://hub.docker.com/r/bitwarden/mssql
[self-db]: https://bitwarden.com/help/external-db/
[xkcd-docker]: https://xkcd.com/1988/
[proxy-doc]: https://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_pass
[normalized]: https://nginx.org/en/docs/http/ngx_http_core_module.html#location
[k8s-nginx]: https://github.com/kubernetes/ingress-nginx/
[vaultwarden]: https://github.com/dani-garcia/vaultwarden
