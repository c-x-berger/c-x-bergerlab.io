title="STOP DOING x86"
tags=['x86', 'intel', 'linux']
::===::
- BRANCHES WERE NOT SUPPOSED TO BE PREDICTED
- YEARS OF MICROARCHITECTURE yet NO REAL-WORLD USE FOUND for COMPLEX INSTRUCTION SETS
- Wanted to have more instructions anyway for a laugh? We had a tool for that: It was called **"ASICs"**
- "Yes please give me TURING COMPLETE `mov`. Please give me 100+ INSTRUCTIONS IN FLIGHT" - Statements dreamed up by the utterly deranged

LOOK at what Intel have been demanding your Respect for all this time, with all the compilers and process nodes we built for them

**(This is REAL x86, done by REAL Intel engineers)**

<div style="display: flex; justify-content: space-around; align-items: center;">
  <figure class="polaroid">
    <img src="/img/knl-errata.png" style="max-width: 450px;">
    <figcaption>????????</figcaption>
  </figure>
  <a href="https://www.jookia.org/wiki/Nopl">
    <figure class="polaroid">
      <img src="/img/nop-madness.png" style="max-height: 100px;">
      <figcaption>????????</figcaption>
    </figure>
  </a>
  <figure class="polaroid">
    <img src="/img/spectre.min.svg" style="min-width: 125px; max-height: 200px;">
    <figcaption>‽‽‽‽‽‽‽‽</figcaption>
  </figure>
</div>

"Hello I would like `vrsqrtps  %ymm1, %ymm2` apples please"

**They have played us for absolute fools**

[nop-fault]: https://web.archive.org/web/20070221081630/http://www.symantec.com/enterprise/security_response/weblog/2007/02/x86_fetchdecode_anomalies.html
[nop-jump]: https://lists.archive.carbon60.com/linux/kernel/1268554
