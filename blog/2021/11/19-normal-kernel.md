title='very normal linux kernel boots on a raspberry pi 4'
subtitle=':)'
tags=['raspberry pi', 'linux', 'bootloader']
::===::
<div class="bad-iframe">
<iframe width="1280" height="720" src="https://www.youtube.com/embed/xBVLTtyfA1U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

If you'd like to make your own Very Normal Linux kernel, it's... surprisingly
complicated. At least, assuming that you don't want to totally clobber the
default logo.

## prepare the logo
1. Create a logo. It should probably be the same size as the default logo - so
somewhere in the neighborhood of 80x80 is good. You'll need to export this from
your [editor of choice][gimp] to a `.ppm` file. Make sure to export it as "raw"
instead of ASCII.
2. Reduce the number of colors in the logo to 224 using `ppmquant`, then convert it to ASCII:
   ```bash
   ppmquant 224 my_boot_logo.ppm > logo_224.ppm
   pnmnoraw logo_224.ppm > logo_clut224.ppm
   ```
   There seems to be a hard requirement that the logo's filename ends in `_clut224.ppm`.

   You can get `ppmquant` and pnmnoraw as part of [`netpbm`] on Arch. Other distributions
   probably have similar packages.

If you just want to _replace_ the default logo, you're basically done. You just
need to replace `drivers/video/logo/logo_linux_clut224.ppm` with your
masterpiece and go through whatever your kernel build and install process is.

## add your masterpiece to the kernel
In order to get your logo as an option when doing `make menuconfig` (like the
special logos for DEC, SPARC, and SuperH no I'm not jealous), you need to alter
a few files.

### `drivers/video/logo/Kconfig`
Here, you're basically just adding the menu option for your logo. Inside the
`if LOGO` block, add something like this:

```kconfig
config LOGO_COOL_CLUT224
	bool "Cooler 224-color Linux logo"
	default n
```
This just sets up the option in Kconfig. Actually _doing_ anything with it is
on the Makefiles and C source files.

### `drivers/video/logo/Makefile`
Towards the top of the file, add a line like this:
```makefile
obj-$(CONFIG_LOGO_COOL_CLUT224) += logo_cool_clut224.o
```
A few things to keep in mind here:
- The `.o` file must have the same "base name" as your logo's `.ppm` file. If
your logo is called `my_neat_logo_clut224.ppm`, the `.o` side of the expression
**must** be `my_neat_logo_clut224.o`.
- Similarly, the `CONFIG_` reference needs to match what was added to `Kconfig`.

### `drivers/video/logo/logo.c`
**Be very careful here.** There's no compile-time check to save you if you
accidentally return your 224 color masterpiece to a 1-bit color framebuffer.

You should find an `#ifdef` block for the default logo:
```c
#ifdef CONFIG_LOGO_LINUX_CLUT224
		/* Generic Linux logo */
		logo = &logo_linux_clut224;
#endif
```
Just below this block, add your new logo:
```c
#ifdef CONFIG_LOGO_LINUX_CLUT224
		/* Generic Linux logo */
		logo = &logo_linux_clut224;
#endif
#ifdef CONFIG_LOGO_COOL_CLUT224
		/* very cool logo */
		logo = &logo_cool_clut224;
#endif
```
Here again you **must** match the base name of your logo file. The `CONFIG_`
name needs to match Kconfig just like in the Makefile.

### `include/linux/linux_logo.h`
Last bit of kernel tinkering. Just add this line, after the definition of
`struct linux_logo`:
```c
extern const struct linux_logo logo_cool_clut224;
```
Again, match the names, you know the drill.

## build with your beautiful new logo
When you `make menuconfig`, you'll find your new logo under Device Drivers ->
Graphics Support -> Bootup logo. Somewhat confusingly, "Bootup logo" is both an
option and a submenu.

You should see your logo listed, though if you've been following these
instructions _exactly_ it will be disabled by default. Go ahead and enable it
and disable the default one. At this point, you just need to build and install
your Brand New Very Cool and Good kernel. (I have instructions on doing this
[for the Raspberry Pi 4,][rpi4-notes] by the way.)

Enjoy making Linux boot with several copies of your distro logo or Steve
Ballmer's face with glowing red eyes or whatever. It's a relatively easy way to
curse a perfectly fine system at the very lowest (software) level.

[gimp]: https://www.gimp.org/
[`netpbm`]: https://archlinux.org/packages/extra/x86_64/netpbm/
[rpi4-notes]: /blog/2021/11/14-pi-boot.html
