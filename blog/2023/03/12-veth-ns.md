title="Publicly routing a \"container\""
subtitle="cOnTaInErS aLwAyS uSe NaT"
date=2023-03-12
tags=['linux', 'routing', 'vultr', 'containers', 'IPv6', 'namespaces', 'networking']
::===::
It's high time I wrote [even more notes that only I care about][pi-notes] in
here. Today's topic: containers (well, namespaces) and IPv6. Without NAT,
because NAT is Literally The Devil Himself.

The goal here today is to create a network namespace with an IPv6 address not
present in the "host" that we can treat as if it were a "real" host. Lots of
[public clouds such as Vultr][vultr] (ref link) route something like a /64 to
each VM that you can use for namespacing like this.

Processes running "inside" the network namespace have access only to the
resources within that namespace, and are mostly shielded from interference from
outside. (A process with `CAP_SYS_ADMIN` in both its current user namespace and
the user namespace associated with a network namespace can freely enter with
`setns(2)`, which is how things like `ip netns exec` work. In practice, this
means something like `sudo` access.)

IPv4 addresses are extremely limited in 2023, so you don't often get a real,
continuous subnet routed to a node - which you'll need to get traffic flowing
properly with these notes. If you're interesed in v4 anyway, [you can usually
NAT your way out of the problem.][v4-ex]

## creationism
The "canonical" way to create a new network namespace for quick and dirty work
short of a "full" container is `ip netns add [name]`, e.g. `ip netns add
my-very-cool-container`. While namespaces cease to exist when they contain no
processes by default, they can be made to stick around with a bind mount - which
is what `ip` is doing here.
```
$ sudo ip netns add meme
$ mount -l | grep meme
nsfs on /run/netns/meme type nsfs (rw)
nsfs on /run/netns/meme type nsfs (rw)
```
(I'm not entirely sure why there's two mounts listed here. If you know why, do tell!)

We can now run commands inside our new network namespace with `ip netns exec`,
e.g. `ip netns exec testing ip a` executes `ip a` inside of the `testing`
namespace.

You can also make a new network namespace with `unshare -n`. This doesn't get
you a bind mount by default (you can make it yourself), but it _does_
immediately drop you into a shell in the new namespace.

If you combine this with a new user namespace and map the current UID to UID 0
(e.g. `unshare -Urn`), the new networking namespace will be "owned" by the newly
created user namespace, which in practice means you can start executing `ip`
stuff inside the namespace without throwing `sudo` on everything. It is a little
trickier to refer to use namespaces created by `unshare(1)` with `ip`. You can
usually use the PID of a process inside the namespace. In Bash's case, `$$`
should expand to the PID of the shell, so you can just `echo $$` to find that.

You can "register" a namespace made this way with `ip`'s `netns` commands with
`ip netns attach [ name ] [ pid ]`.

### tabula rasa
A new network namespace is, functionally, a clean-slate copy of the entire
networking stack. No firewall rules, no routes, no nothing.

```
$ unshare -Urn # set up a new user namespace, map UID 0
[inside] $ ip a
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
```

The following commands all output nothing:
```
[inside] $ ip route
[inside] $ nft list tables
```

And if you still have `iptables` or a shim for it, there's nothing there either.
We still need UID 0 access to the filesystem in order to use `iptables` (at
least, on this machine), and the easiest way to have both that and the netns is
with some `ip netns exec`.

```bash
# attach the namespace created with unshare to ip's bookkeeping
sudo ip netns attach testing $PID_OF_UNSHARED_SHELL
sudo ip netns exec testing iptables -L -v
```
```
Chain INPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination

Chain OUTPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination
```

On to actually using this.

## fake cables
There's a few ways to get traffic "out" of a network namespace (and hopefully,
towards the Internet). The most direct way is a virtual Ethernet -
[`veth(4)`][man-veth] - device. `veth` devices are sort of like two physical
NICs plugged into each other. They can thus only exist in pairs, with any
traffic transmitted by one end immediately recieved on the other. If you already
have a network namespace, you can pick what namespaces the devices are created
in:
```bash
# here we're creating the devices in the root NS - so we can leave one of the
# devices in the current namespace, and put the other end in our "testing"
# namespace from before
# create a pair of veth devices veth1 and vpeer1, placing vpeer1 in the "testing" netns
sudo ip link add veth1 type veth peer vpeer1 netns testing
```
Alternatively, you can move any device (not just `veth`s!) into some namespace
with `ip link set [ dev ] netns [ ns ]`.

### my address is 123 main street
If we inspect our namespace now, we can see that it has the `vpeer1` device, but
there are no addresses on it.

```bash
# for clarity, we'll stick to ip netns exec, and let's go ahead and set lo up too
# so all of this needs CAP_SYS_ADMIN on both sides, i.e. whack sudo on the front of it
ip netns exec testing ip link set lo up
ip netns exec testing ip a
```
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: vpeer1@if10: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether b6:bb:91:55:de:1d brd ff:ff:ff:ff:ff:ff link-netnsid 0
```

Let's go ahead and slap an IPv6 address on one end of the pair and see what happens.

```bash
ip netns exec testing ip addr add dev vpeer1 2001:0db8::c0
ip netns exec testing ip -6 route
```
```
2001:db8::c0 dev vpeer1 proto kernel metric 256 linkdown pref medium
```

Except, we can't even ping ourselves yet. We need to set _both_ ends of the link
up before that will work:
```bash
ip netns exec testing ip link set vpeer1 up
ip link set veth1 up
```

May as well add an address to the other end while we're at it.
```bash
ip addr add dev veth1 2001:0db8::1
```

### crossing the border
We can't reach between the two interfaces of our veth pair yet. Technically, we
_can_ use the link-local addresses, but our globally routable addresses won't do
any good yet. To fix that, we need to set up a little bit of routing between the
two.

A neat trick for doing this "automatically" is to set a more reasonable subnet
mask on these addresses - in this case, setting them both to be a 64-bit mask
results in the two interfaces finding each other other plain old NDP. Yes,
really, NDP - you can check it with `tcpdump` if you like.

But since we made our bed with these default 128-bit masks, we may as well lie
in it.

```bash
# add a route to the veth device inside our namespace
ip netns exec testing ip route add 2001:db8::1 dev vpeer1
# and vice versa
ip route add 2001:db8::c0 dev veth1
```

### out to infinity
And now we can `ping`! But we still can't reach the Internet - we have no
default route in the `testing` namespace, so there's no path to anything that's
not one of the two veth devices. Adding a default route fixes that up:

```bash
ip netns exec testing ip -6 route add devault dev vpeer1 via 2001:db8::1
```

(You still might not be able to ping out if forwarding is disabled on `veth1`.
Try `sysctl -w net.ipv6.conf.all.forwarding=1` in `veth1`'s namespace.)

## a note on DNS
Interestingly, while you might reasonably assume that DNS is part of networking
and thus part of the network namespace, it's actually controlled by the
_filesystem_ - specifically, `/etc/resolv.conf` on your average glibc-using
system. Getting the content you want into that file is a whole mess of
NetworkManager or `systemd-resolved` or some other software package - but if DNS
seems broken while everything else is fine, that's _probably_ why.

## other ways
There are some other, more complex methods to get traffic out of a namespace and
onto the wire. A particularly interesting one is WireGuard: a WireGuard
interface remembers where it was created, and always sends its encrypted UDP
traffic "from" that namespace. For example, if you create a WireGuard interface
in the root network namespace, then move it into some container's namespace as
the only interface with actual routing, all traffic from that container will be
sent through the WireGuard tunnel.

WireGuard's website [has much more detail about this feature and what it
enables.][wg-ns]

[pi-notes]: https://c-x-berger.gitlab.io/blog/2021/11/14-pi-boot.html
[vultr]: https://www.vultr.com/?ref=8849494
[v4-ex]: https://blogs.igalia.com/dpino/2016/04/10/network-namespaces/
[man-veth]: https://man7.org/linux/man-pages/man4/veth.4.html
[wg-ns]: https://www.wireguard.com/netns/
