title="Full-Body VR Tracking on Linux"
subtitle="penguins in the Tundra"
description='did you know that the wikipedia page for T-pose contains a link to Crucifixion because you do now'
date=2023-04-04
tags=['linux', 'openvr', 'steamvr', 'vr', 'Tundra Labs']
::===::
I recently came into posession of a [really cool rock.][] Four of them,
actually. (Anyone know how to get a standard camera mount to stick out of one's
sternum?)

Full-body tracking for VR is incredibly niche. It's an expensive add-on (~$130
per tracker, and you need *at least* three of them) to an already expensive
medium: ~$400 for a Quest 2, which doesn't really support full-body tracking in
its standalone mode, so you need a ~$600+ Gaming™ PC to drive it - and wait, the
full-body trackers on the market today use outside-in tracking, so you need at
least base stations that are perputually out of stock and cost $200 each. (You
can dodge the base stations being out of stock by buying the $1000 Valve Index
kit that comes with two of the things.)

And once you have all that set up, there's like, three games on Steam that
actually use them.

But, despite all of that, it's still somehow worth it. It's really, really,
_really_ gratifying to be able to kick someone in the head in VRChat (or dance
or lie down correctly I guess). At least, if you're hosting the same species of
brain worms that I am. But if you're one of those people, you're also _really_
likely to not be running Windows. So how much of a pain in the neck is it to get
your shiny new ankle monitors to play nice with SteamVR on Linux?

## a control case
Let's start with a sanity check: how painful is this process on Windows?

After shoving Tundra Labs' SW4 in the front compartment of my Index, getting
three of my trackers charged and screwed on to straps (one for each foot and one
for my waist), and powering the whole thing up, SteamVR immediately detects the
new lot of hardware and I can see the trackers represented with an accurately
tracked 3D model in SteamVR's system menus (the same way the Knuckles
controllers are). The tricky part is setting up the "role" of each tracker.
While things like Knuckles controllers are essentially hard-coded to be your
left and right hands, generic trackers need to be explicitly told what they
represent.

The tricky part is figuring out which tracker you're assigning a role to.
SteamVR essentially gives you a list of serial numbers and asks you what they
should represent. There _is_ a button labeled "identify" for each tracker in the
UI, but that doesn't actually seem to do anything? Maybe I'm just missing
something, but it doesn't seem to cause an LED blink or anything, and Tundra
Labs' trackers don't have their serial numbers printed anywhere easily visible.
(Maybe it's on the PCB or something, but I didn't crack mine open to check.)
This means the best strategy for configuring your trackers is to turn them on
and configure them one at a time, being careful not to overwrite your previous
configuration.

<figure class="polaroid">

![The SteamVR tracker menu.](/img/tracker-menu.png)
<figcaption>The SteamVR tracker menu.</figcaption>
</figure>

Once all that's done, you can hop into VRChat, click "Calibrate FBT" in the
launchpad, ~~assert dominance over desktop and Quest users~~ stand in a [t-pose]
with your real body to line up with your avatar, then click the triggers on your
handheld controllers and _poof_: you have legs!

### site incident review
Well, ok, that was pretty straight forward. Tundra Labs' hardware is a pretty
well-trodden path, so it's not surprising that getting this going went well
overall. Configuring which tracker is which body part kinda sucks, but we didn't
have to do something stupid like buying a high-end USB PCI-e card (_cough Linus
Sebastian cough_).

## on the flip side
Reboot. Welcome back to Linux. Feels like home.

Thankfully, the trackers are bonded to the SW dongle specifically, so we don't
have to fuss with re-pairing every time we reboot or [getting the same keys on
both "systems" or something.][bt-dualboot] And the UI for configuring the
trackers is (modulo the SteamVR web helper being... strange) the same as it was
on Windows.

Well, it _looks_ the same. Only problem is that it doesn't actually _work_.
Selecting a role for some tracker from the dropdown works _maybe_ 10% of the
time. The rest of the time, the input just gets stubbornly eaten by SteamVR,
leaving you sad and your trackers useless. There's an [open issue about this on
`ValveSoftware/SteamVR-for-Linux`][github-tracker-issue], but who knows when or
if Valve will take a look at it. In the meantime, you, dear reader, have two
options:
- Repeatedly try to configure each tracker until it randomly works
- Crack open `~/.steam/steam/config/steamvr.vrsettings` in your text editor of choice and _fix it._

### fixing it
Some trawling through my Windows installation revealed how SteamVR keeps track
of what tracker is what: in `steamvr.vrsettings`, there's a `trackers` key which
maps each tracker to its role. It looks like this:

```json
{
  "trackers": {
    "/devices/lighthouse/LHR-XXXXXXXX" : "TrackerRole_LeftFoot"
  }
}
```

Each tracker has its own entry in this mapping. The various `TrackerRole`s _appear_ to be:
- `TrackerRole_Handed`
- `TrackerRole_LeftFoot`
- `TrackerRole_RightFoot`
- `TrackerRole_LeftShoulder`
- `TrackerRole_RightShoulder`
- `TrackerRole_LeftElbow`
- `TrackerRole_RightElbow`
- `TrackerRole_LeftKnee`
- `TrackerRole_RightKnee`
- `TrackerRole_Waist`
- `TrackerRole_Chest`
- `TrackerRole_Camera`
- `TrackerRole_Keyboard`

Each of these neatly corresponds to one of the tracker roles offered in the
SteamVR UI. From here, it's pretty straightforward to get things labeled as we
like:

```json
{
  "trackers": {
    "/devices/lighthouse/LHR-XXXXXXXX" : "TrackerRole_LeftFoot",
    "/devices/lighthouse/LHR-XXXXXXXX" : "TrackerRole_RightFoot",
    "/devices/lighthouse/LHR-XXXXXXXX" : "TrackerRole_Waist"
  }
}
```
Yours may be in a different order. It's JSON, it all comes out in the wash.

## conclusion
Honestly, this works _way_ better than I expected. Of course, I'd like it if
SteamVR's UI did its job properly, but a broken GUI that can be easily
sidestepped by editing one simple config file is far from the _worst_ user
experience this could have been. And once that's done, everything Just Works
like it did on Windows!

Regardless of platform, find some way to keep your left and right foot trackers
from getting mixed up. I keep mine wrapped around the matching Knuckles
controllers, but you could also try something like labelling the strap (**not**
the tracker itself, you don't want to cover one of the sensors) or getting the
[$5 strap loop baseplates][loop-plate] and threading them onto a cheap pair of
shoes or something ([e.g. ThrillSeeker][]).

I'm interested in exploring other options as well - [SlimeVR][] in particular looks
like a good, low-cost way to get trackers for other track points. And if any of
that happens, ~~we'll ask you about it on _Wait Wait, Don't Tell Me_~~ I'll be
sure to write it up as well.

[really cool rock.]: https://mastodon.boiler.social/@cxberger/110137581269083149
[t-pose]: https://en.wikipedia.org/wiki/T-pose
[bt-dualboot]: https://wiki.archlinux.org/title/Bluetooth#Dual_boot_pairing
[github-tracker-issue]: https://github.com/ValveSoftware/SteamVR-for-Linux/issues/556
[loop-plate]: https://tundra-labs.com/products/basic-straps
[e.g. ThrillSeeker]: https://youtu.be/bohzJkvHHqc?t=510
[SlimeVR]: https://slimevr.dev
