title="The Federated Timeline is an Anti-Feature"
subtitle="and should be removed or disabled"
description="Look, I'm not happy about it either."
date=2023-02-15
tags=['ActivityPub', 'Mastodon', 'fedi', 'fediverse', 'federation']
::===::
A recent happening which shall remain nameless prompted me to think about
Mastodon and its cousins again. I decided to actually write something about it
this time, because if I didn't I knew I'd forget.

## a thesis statement
The federated timeline present in Mastodon, Pleroma, and the various
descendants thereof is a **terrible** feature which is _bad for the Fediverse
and its users._ It should be removed, or at bare minimum put behind a
disabled-by-default config option, unless and until we figure out a better
moderation model than "keep tabs on all the happenings everywhere or you fail,
and maybe distribute lolicon."

I wish I was overstating the case.

![Screenshot of eight instances on poa.st's blocklist, all with reasons
involving pedophilia. The actual domain names have been
redacted.](/img/poa.st-blocks.png)

(These are just the ones at the top of the list. Yes, I know poa.st and the
instances in its vicinity are all insane, but as we'll see in a moment, even if
your instance is "good" this should probably still spook you a little.)

## the important parts
Given that I'm about to hopefully tear to shreds what I recognize some people
believe is an important part of Mastodon (and other ActivityPub
implementations), it seems wise to open with a few possible mis-readings,
following the style of the [List of Common
Misconceptions][misconceptions-list], where only the true thing is stated and
the incorrect reading left implied.

- I *like* fedi. I think it's a fundamental improvement over the totally
  centralized "the entire Web is maybe four websites run to enrich people with
  more money than God almighty, and all of them are mostly just posting about
  each other" model we've all been made to suffer for the last five years or
  so.
- The solution to the ills of social media probably isn't yet another closed
  source, centralized platform. You know who you are.
- The federated timeline is _far_ from the only abuse vector in Mastodon.

  It's just the most glaringly stupid one.
- `boiler.social` isn't going to kill off its Mastodon instance, nor do I
  intend to abdicate my position as Dictator Until the End of Time on there -
  at least, not until I have someone better to run it. That won't be for a
  while yet though.

Good? Good.

## where do we even begin
For the whole one of you who doesn't already know where I'm going, the
federated timeline is a... "feature" of Mastodon, Akkoma, and several other
ActivityPub-based "Twitter but not" implementations which, keeping with
comparisons to Elon Musk's latest disaster piece, is "the firehose". It's
*everything* the instance sees, *for any reason*. Reasons content might appear
on the federated timeline include, but probably aren't limited to:
- You or someone else on your instance posted it.
- You or someone else on your instance follow(s) the user that posted it.
- You or someone else on your instance boosted, favorited, or replied to it.
   - Replies can get a surprising number of things "pulled in". When you load a
     reply to a post, as far as I can tell, Mastodon will attempt to load the
     thing that was replied to, and might actually keep doing so until it gets
     to the "start of the thread" or something. I'm not *entirely* sure about
     this.
- You or someone else on your instance searched for it by URL.
   - It actually looks like at least Akkoma will let *anyone* pull things in
     this way, signed in or not. That's nice.
- [Something that looks vaguely like a real instance sent your instance an HTTP
  POST.][minimum-post]
- Your instance and the instance that originated the content are on the same
  "relay".

  Relays are servers which recieve posts from participating instances, and
  broadcast the content out to other instances participating in the relay. Some
  relays have an approval process, some are just public. I'm not aware of a way
  to determine what relays your instance participates in as a normal user
  without access to the admin panels.
- Probably other ways I have forgotten or don't know about!

The point here is _wow that's a lot of ways for content to get there._ This
alone isn't really a problem for me - instances getting content lots of
different ways makes it a bit annoying to answer "how did this get here"
(because of course, none of these implementations bother to have that written
down anywhere), but "instances see lots of things" isn't a problem unless
you're short on disk space. And there's retention policies to ease that
headache a little anyway.

Or at least, it wouldn't be.

## the elephant through time in the room
So instances have all this content coming in, from all over the fediverse, in
ways that, if we're being honest with ourselves, can be pretty unpredictable.
Cool, cool.

And then someone decided to build a feature that shows all of it. In real time.
To anyone logged in. [Or not, if you have a config option set
true.](https://mastodon.social/public) (I genuinely don't remember if this is
the default, and I can't seem to find docs on it either. Also, in complete
fairness, signed-out users don't get real-time updates.)

I feel like I'm taking goddamn crazy pills. <!-- No wonder large instances are
heaving along like Bagger 288. Not only do they have thousands of users who
never stop talking, and have just as many other instances posting to them at
all hours, they have to be ready to supply the entire firehose to any of thier
users who ask for it! And when you're mastodon.social, that firehose is a
half-decent approximation of _the entire fediverse!_ I'd link the post someone
made about Mastodon replicating the "Ruby service with capacity problems" part
of young Twitter someone made a month or so back, but, well, I'd have to search
for it, and that's a whole other bag of takes we **will not be opening** today.
-->

If you have not yet appreciated how completely and utterly deranged this is, I
fear there is no way I can impress it upon you, but I will try nevertheless.

### a list of various nonsense I have personally dealt with
- Various posters from large instances who, being charitable, forgot to tag
  their porn as sensitive
   - At least one user who _definitely_ wasn't forgetting
- A wide assortment of racists, anti-semites, \*phobes, and so on
- A single-user instance with an account titled "Deus Vult"
   - ... who, having checked in for the purposes of this blog post, is happily
     boosting people like "Neigh-Sayer the N\*\*\*\*\* Slayer" 
- [Whatever the hell this crap is supposed to be][pedo-poll]
   - "ironic" pedophilia is still pedophilia, you inexcusable waste of oxygen
   - a few other vile things crawled out of that pit before I quickly wisened
     up and blocked `bae.st` entirely

I did try to figure out where some of the more egregious violations of common
decency were coming from. I wasn't following these chuds, and none of my users
appeared to be either. Maybe they snuck in via relay? Maybe a scraper picked up
on `boiler.social`'s existence and started delivery? I really have no clue, and
I don't know if I ever will.

Now that most of the blaringly obvious garbage has been blocked, I admit the
timelines are _mostly_ free of things that make me want to hurl. I'm sure
there's still some in there, more than there should be, but it's infrequent
enough that I'm not seeing it and nobody's reporting it, so... victory? Until a
new swath of garbage instances comes around, I guess?

Yes, just having this on a service I run feels gross. Of course I would rather
not have this sitting on a drive I own and control. But the _main_ reason this
content is a problem for myself and my end-users is because there's on on-tap
firehose of everything, from everywhere, all at once. Otherwise, yes, it might
be discoverable through a search (lol) gone wrong or something, but it
certainly wouldn't be a situation where a particularly unlucky user logs in (or
worse, a no-account guest comes by), passively checks the various feeds
Mastodon offers, and is swiftly greeted with the worst content the server will
see that year.

### the local timeline
From a technical perspective, I'm not entirely pleased with the local timeline
either. It still smells like a firehose waiting to happen as an instance grows.
But at least from the pure moderation perspective, the local timeline
exclusively contains content posted or amplified by _my instance's users_ -
people I chose to be hosting and thus at least a little responsible for. Saying
the local timeline should be clean is, to me, a fair expectation.

This line of thinking obviously only goes so far, which is how we got things
like CDA Section 230, but for most ActivityPub nodes out there this is
basically true. It's certainly true of `boiler.social` anyway.

## `#Fediblock` is a symptom, not a cure
Now we're *really* in hot-take land.

One of the band-aids for this madness is the `#Fediblock` hashtag. This is
exactly what it sounds like - a tag used to raise the alarm about Happenings
which instance admins might want to take action on (and by "take action", I
usually mean "blocking the instance where the bad stuff is".)

Whether or not you think this is _useful_ is at least partially a personal
values thing. On the one hand, you do get the ease of basically outsourcing
your moderation to other people. On the other, uh, _you're outsourcing your
moderation to other people_. Hopefully nobody feels like [just going on the
Internet and telling lies?][lies] As far as I can tell, this is vanishingly
uncommon, but at least to me, it doesn't seem wise to blindly follow the advice
of Internet strangers... and the work of verifying what they say feels sort of
like a return to square one.

Whether you find `#Fediblock` useful or not though, it's really not a proper
solution. The problem here is some part "instances with unpleasant policy and
users exist", and another (significant) part "well-meaning instances will
happily surface unpleasant content to users unless configured otherwise".
`#Fediblock` is a middling, reactive aid for admins trying to get a handle on
the first part of this issue without making "staring at the federated timeline"
a full-time job. I'm glad it's there, if only because it's better than nothing
at all, but "better than nothing" is a pathetically low bar to have cleared
after six years.

## what about [thing] though?
I don't expect getting rid of the federated timeline to be a perfect solution.

Yes, it would get rid of the laziest kind of discoverability. You'd have to
actually know what you want and search for it with hashtags instead of waiting
for something tasty-looking to come down the digital sushi conveyor. The utter
horror. (Besides, try actually finding good content in there sometime. It gets
worse and worse for discovering interesting stuff the more active your instance
and its "neighbors" get, unless your admin is intentionally limiting what you
federate with for some reason.)

Yes, people who want to be horrible could still dump gore into `#baking` or
whatever. Or DM users with it. Or follow people from accounts that have weirdly
racist caricatures for profile pictures. At least these aren't _new_ problems
we decided to create from the void, though. They're by no means solved, sure,
but they're not new additions to the pile.

## you're taking this too seriously, you have like. ten users.
Maybe. I'm known to do that.

The federated timeline is:
- unique to Mastodon and its various relatives
- not particularly valuable for anything: finding stuff should be served well
  enough by tag search, or even mediocre full-text search
- a novel way for unsolicited, undesirable content to surface to unsuspecting
  users
- asks instance admins and moderators \- [usually volunteers][babs-and-maureen]
  \- to deal with the totality of the visible fediverse 

It should go.

[misconceptions-list]: https://en.wikipedia.org/wiki/List_of_common_misconceptions
[minimum-post]: https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/
[pedo-poll]: /img/bae.st-pedos.png
[lies]: https://www.youtube.com/watch?v=YWdD206eSv0
[babs-and-maureen]: https://mastodon.social/@Richard_Littler/109364310000292806
